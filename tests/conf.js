'use strict';

let SpecReporter = require('jasmine-spec-reporter').SpecReporter;
require('protractor-console');

exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',

  capabilities: {
    browserName: 'chrome',
    // Run tests in a headless Chrome
    // https://github.com/angular/protractor/blob/master/docs/browser-setup.md#using-headless-chrome
    chromeOptions: {
      args: [
        // IMPORTANT: Required flag for running Chrome in unprivileged Docker,
        // see https://github.com/karma-runner/karma-chrome-launcher/issues/125#issuecomment-312668593
        '--no-sandbox',
        '--headless',
        '--disable-gpu'
        ]
      }
    },

  suites: {
    login: 'e2e/login/**/*.spec.js',
    /* Disabled for a while */
    //users: 'e2e/users/**/*.spec.js'
  },
  jasmineNodeOpts: {
    showColors: true, // Use colors in the command line report.
    includeStackTrace: false,
    isVerbose : true,
    print: function() {}
  },
  onPrepare: function () {
    jasmine.getEnv().addReporter(new SpecReporter({
      spec: {
        displayStacktrace: true
      }
    }));
  },
  plugins: [{
    package: 'protractor-console',
    logLevels: []
  }]
}
