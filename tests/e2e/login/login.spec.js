// spec.js

var helper = require('../helper');
var data = require('../data');
var LoginPage = require('./login.page');

describe('Login Page', function() {
  var page = new LoginPage();

  beforeEach(function() {
    page.visit();
  });

  describe('user', function() {
    it('should require login and email', function() {
      browser.sleep(1000); //sleep because initial info toast

      page.login({
        password: '',
        email: ''
      });

      helper.expectToastToEqual('The Email field is required.\nThe Password field is required.');
    });

    it('shouldn\'t authenticated with invalid credentials', function() {
      browser.sleep(1000); //sleep because inital info toast

      page.login({
        password: 'invalide-password'
      });

      helper.expectToastToEqual('Invalid credentials');
    });

    it('when try access authenticated page should be redirected to login page if not logged in', function() {
      browser.get(data.domain + 'app/users');

      expect(browser.getCurrentUrl()).toMatch(page.loginUrl);
    });

    it('should authenticated with valid credentials', function() {
      page.login();
      //check the shown email
      // browser.getPageSource().then(function (html) {
      //   console.log(html);
      // });
      expect(element(by.id('logged-user-mail')).getAttribute('textContent')).toEqual(data.validAdminUser.email);
    });
  });
});
