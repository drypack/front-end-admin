/*eslint angular/file-name: 0, no-undef: 0*/
(function() {
  'use strict';

  angular
    .module('app')
    .constant('en-US.i18n.dialog', {
      confirmTitle: 'Confirmation',
      confirmDescription: 'Confirm the action?',
      removeDescription: 'Do you want to remove permanently {{name}}?',
      audit: {
        created: 'Record information',
        updatedBefore: 'Before update',
        updatedAfter: 'After update',
        deleted: 'Information before remove'
      },
      login: {
        resetPassword: {
          description: 'Type below the email registered on the system.'
        }
      }
    })

}());
