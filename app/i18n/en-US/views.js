/*eslint angular/file-name: 0, no-undef: 0*/
(function () {
  'use strict';

  angular
    .module('app')
    .constant('en-US.i18n.views', {
      breadcrumbs: {
        user: 'Administration - User',
        'user-profile': 'Profile',
        dashboard: 'Dashboard',
        audit: 'Administration - Audit',
        mail: 'Administration - Mail sending',
        project: 'Examples - Projects',
        'dynamic-query': 'Administration - Dynamic Query',
        'not-authorized': 'Access Denied',
        roles: 'Roles'
      },
      titles: {
        dashboard: 'Start page',
        mailSend: 'Send e-mail',
        taskList: 'Task list',
        userList: 'User list',
        auditList: 'Audit log list',
        register: 'Registration form',
        resetPassword: 'Reset password',
        update: 'Update form',
        roles: 'Roles'
      },
      actions: {
        send: 'Send',
        save: 'Save',
        clear: 'Clear',
        clearAll: 'Clear all',
        restart: 'Restart',
        filter: 'Filter',
        search: 'Search',
        list: 'List',
        edit: 'Edit',
        cancel: 'Cancel',
        update: 'Update',
        remove: 'Remove',
        getOut: 'Exit',
        add: 'Add',
        login: 'Login',
        loadImage: 'Load image',
        manageTasks: 'Manage tasks',
        cantRemove: 'Can not be removed',
        close: 'Close'
      },
      fields: {
        date: 'Date',
        action: 'Action',
        actions: 'Actions',
        event: 'Event',
        audit: {
          dateStart: 'Initial date',
          dateEnd: 'Ending date',
          resource: 'Resource',
          allResources: 'All resources',
          event: {
            created: 'Created',
            updated: 'Updated',
            deleted: 'Removed'
          }
        },
        login: {
          resetPassword: 'I have forgot my password',
          confirmPassword: 'Confirm password'
        },
        mail: {
          to: 'To',
          subject: 'Subject',
          message: 'Message'
        },
        queryDynamic: {
          filters: 'Filters',
          results: 'Results',
          model: 'Model',
          property: 'Attribute',
          operator: 'Operator',
          resource: 'Resource',
          value: 'Value',
          operators: {
            equals: 'Equal',
            different: 'Different',
            contains: 'Contain',
            startswith: 'Starts with',
            endswith: 'Ends with',
            biggerThan: 'Bigger than',
            equalsOrBiggerThan: 'Bigger or Equal than',
            lessThan: 'Less than',
            equalsOrLessThan: 'Less or Equal than'
          }
        },
        project: {
          name: 'Name',
          cost: 'Cost',
          totalTask: 'Total of Tasks'
        },
        task: {
          done: 'Not Done / Done'
        },
        user: {
          profiles: 'Profiles',
          nameOrEmail: 'Name or Email'
        }
      },
      layout: {
        menu: {
          dashboard: 'Dashboard',
          project: 'Projects',
          admin: 'Administration',
          examples: 'Examples',
          user: 'Users',
          mail: 'Send e-mail',
          audit: 'Audit',
          dynamicQuery: 'Dynamic Queries',
          roles: 'Roles'
        },
        attention: 'Attention',
        browserNotSupported: 'Browser not supported'
      },
      locale: {
        locale: 'Locale',
        change: 'Change locale'
      },
      tooltips: {
        audit: {
          viewDetail: 'View details'
        },
        user: {
          profile: 'Profile',
          transfer: 'Transfer'
        },
        task: {
          listTask: 'List Tasks'
        }
      },
      pagination: {
        total: 'Total',
        items: 'Item(s)'
      },
      role: {
        resource: 'Resource',
        permissions: 'Allowed actions'
      }
    })

}());
