/*eslint angular/file-name: 0, no-undef: 0*/
(function() {
  'use strict';

  angular
    .module('app')
    .constant('en-US.i18n.attributes', {
      email: 'Email',
      password: 'Password',
      name: 'Name',
      image: 'Image',
      roles: 'Roles',
      role: 'Role',
      date: 'Date',
      initialDate: 'Initial date',
      finalDate: 'Ending date',
      task: {
        description: 'Description',
        done: 'Done?',
        priority: 'Priority',
        scheduled_to: 'Scheduled to?',
        project: 'Project'
      },
      project: {
        cost: 'Cost'
      },
      // list loaded from the back-end if it is defined in the back-end
      auditModel: {
      }
    })

}());
