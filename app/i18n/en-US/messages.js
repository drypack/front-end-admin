/*eslint angular/file-name: 0, no-undef: 0*/
(function() {
  'use strict';

  angular
    .module('app')
    .constant('en-US.i18n.messages', {
      attention: 'Attention',
      internalError: 'An internal error has occurred, contact the system admin',
      notFound: 'No records found',
      notAuthorized: 'You do not have permission to run this functionality.',
      searchError: 'It was not possible to do the search.',
      saveSuccess: 'Record saved successfully.',
      operationSuccess: 'Operation realized successfully.',
      operationError: 'Error while trying to realize the operation.',
      saveError: 'Error while trying to save the record.',
      removeSuccess: 'Removal done successfully.',
      removeError: 'Error while trying to remove the record.',
      resourceNotFoundError: 'Resource not found',
      notNullError: 'All the mandatory fields must be filled.',
      duplicatedResourceError: 'There is already a resource with the same data.',
      actionDependenciesAutoSelected: 'Permission dependencies selected/unselected automatically.',
      localeSwitchedTo: 'Locale switched to {{locale}}',
      validate: {
        fieldRequired: 'The field {{field}} is mandatory.'
      },
      layout: {
        error404: 'Page not found'
      },
      login: {
        logoutInactive: 'Please login again.',
        invalidCredentials: 'Invalid credentials',
        unknownError: 'It was not possible to login. Try again. In case of problems please contact the system administrator.',
        userNotFound: 'It was not possible to found your data'
      },
      dashboard: {
        welcome: 'Be welcomed {{userName}}',
        description: 'Use the menu for navigation.'
      },
      mail: {
        mailErrors: 'An error has occurred in the following emails:\n',
        sendMailSuccess: 'Email send successfully!',
        sendMailError: 'It was not possible to send the email.',
        passwordSendingSuccess: 'The password recovery process was initiated. If the recovery email do not arrive in 10 minutes try again.'
      },
      user: {
        userExists: 'User already added!',
        profile: {
          updateError: 'It was not possible to update your profile'
        }
      },
      queryDynamic: {
        noFilter: 'No filter was added'
      }
    })

}());
