/*eslint angular/file-name: 0, no-undef: 0*/
(function() {
  'use strict';

  angular
    .module('app')
    .constant('pt-BR.i18n.attributes', {
      email: 'Email',
      password: 'Senha',
      name: 'Nome',
      image: 'Imagem',
      roles: 'Papéis',
      role: 'Papel',
      date: 'Data',
      initialDate: 'Data Inicial',
      finalDate: 'Data Final',
      task: {
        description: 'Descrição',
        done: 'Feito?',
        priority: 'Prioridade',
        scheduled_to: 'Agendado Para?',
        project: 'Projeto'
      },
      project: {
        cost: 'Custo'
      },
      // lis loaded from the back-end if it is defined in the back-end
      auditModel: {
      }
    })

}());
