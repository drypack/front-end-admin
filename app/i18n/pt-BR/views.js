/*eslint angular/file-name: 0, no-undef: 0*/
(function () {
  'use strict';

  angular
    .module('app')
    .constant('pt-BR.i18n.views', {
      breadcrumbs: {
        user: 'Administração - Usuário',
        'user-profile': 'Perfil',
        dashboard: 'Dashboard',
        audit: 'Administração - Auditoria',
        mail: 'Administração - Envio de e-mail',
        project: 'Exemplos - Projetos',
        'dynamic-query': 'Administração - Consultas Dinâmicas',
        'not-authorized': 'Acesso Negado',
        roles: 'Papéis'
      },
      titles: {
        dashboard: 'Página inicial',
        mailSend: 'Enviar e-mail',
        taskList: 'Lista de Tarefas',
        userList: 'Lista de Usuários',
        auditList: 'Lista de Logs',
        register: 'Formulário de Cadastro',
        resetPassword: 'Redefinir Senha',
        update: 'Formulário de Atualização',
        roles: 'Perfis'
      },
      actions: {
        send: 'Enviar',
        save: 'Salvar',
        clear: 'Limpar',
        clearAll: 'Limpar Tudo',
        restart: 'Reiniciar',
        filter: 'Filtrar',
        search: 'Pesquisar',
        list: 'Listar',
        edit: 'Editar',
        cancel: 'Cancelar',
        update: 'Atualizar',
        remove: 'Remover',
        getOut: 'Sair',
        add: 'Adicionar',
        login: 'Entrar',
        loadImage: 'Carregar Imagem',
        manageTasks: 'Gerenciar tarefas',
        cantRemove: 'Não pode ser removido(a)',
        close: 'Fechar'
      },
      fields: {
        date: 'Data',
        action: 'Ação',
        actions: 'Ações',
        event: 'Evento',
        audit: {
          dateStart: 'Data Inicial',
          dateEnd: 'Data Final',
          resource: 'Recurso',
          allResources: 'Todos Recursos',
          event: {
            created: 'Cadastrado',
            updated: 'Atualizado',
            deleted: 'Removido'
          }
        },
        login: {
          resetPassword: 'Esqueci minha senha',
          confirmPassword: 'Confirmar senha'
        },
        mail: {
          to: 'Para',
          subject: 'Assunto',
          message: 'Mensagem'
        },
        queryDynamic: {
          filters: 'Filtros',
          results: 'Resultados',
          model: 'Model',
          property: 'Atributo',
          operator: 'Operador',
          resource: 'Recurso',
          value: 'Valor',
          operators: {
            equals: 'Igual',
            different: 'Diferente',
            contains: 'Contém',
            startswith: 'Inicia com',
            endswith: 'Termina com',
            biggerThan: 'Maior que',
            equalsOrBiggerThan: 'Maior ou Igual a',
            lessThan: 'Menor que',
            equalsOrLessThan: 'Menor ou Igual a'
          }
        },
        project: {
          name: 'Nome',
          cost: 'Custo',
          totalTask: 'Total de tarefas'
        },
        task: {
          done: 'Não Feito / Feito'
        },
        user: {
          profiles: 'Perfis',
          nameOrEmail: 'Nome ou Email'
        }
      },
      layout: {
        menu: {
          dashboard: 'Dashboard',
          project: 'Projetos',
          admin: 'Administração',
          examples: 'Exemplos',
          user: 'Usuários',
          mail: 'Enviar e-mail',
          audit: 'Auditoria',
          dynamicQuery: 'Consultas Dinamicas',
          roles: 'Papéis'
        },
        attention: 'ATENÇÃO',
        browserNotSupported: 'Navegador não Suportado'
      },
      locale: {
        locale: 'Locale',
        change: 'Change locale'
      },
      tooltips: {
        audit: {
          viewDetail: 'Visualizar Detalhamento'
        },
        user: {
          profile: 'Perfil',
          transfer: 'Transferir'
        },
        task: {
          listTask: 'Listar Tarefas'
        }
      },
      pagination: {
        total: 'Total',
        items: 'Item(s)'
      },
      role: {
        resource: 'Recurso',
        permissions: 'Ações permitidas'
      }
    })

}());
