(function() {

  'use strict';

  angular
    .module('app')
    .filter('tBreadcrumb', tBreadcrumb);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function tBreadcrumb($filter) {
    /**
     * Filter for translating the breadcrumb
     *
     * @param {any} id state reference key
     * @returns the translated attribute or the original value passed, if not found
     */
    return function(id) {
      // Takes the second parte of the state key, removing the abstract parte (app.)
      var key = 'views.breadcrumbs.' + id.split('.')[1];
      var translate = $filter('translate')(key);

      return (translate === key) ? id : translate;
    }
  }

})();
