(function() {

  'use strict';

  angular
    .module('app')
    .filter('tModel', tModel);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function tModel($filter) {
    /**
     * Filter for translating a model attribute
     *
     * @param {any} name attribute name
     * @returns the translated attribute or the original value passed, if not found
     */
    return function(name) {
      var key = 'models.' + name.toLowerCase();
      var translate = $filter('translate')(key);

      return (translate === key) ? name : translate;
    }
  }

})();
