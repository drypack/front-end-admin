(function() {
  'use strict';

  angular
    .module('app')
    .config(routes);

  /** @ngInject */
  function routes($stateProvider, $urlRouterProvider, Global) {
    $stateProvider
      .state('app', {
        url: '/app',
        templateUrl: Global.clientPath + '/shared/app.html',
        abstract: true,
        resolve: { // ensure langs is ready before render view
          translateReady: ['$injector', function($injector) {
            // return a promise
            return $injector.get('LocaleService').setLocale();
          }]
        }
      })
      .state(Global.notAuthorizedState, {
        url: '/access-denied',
        templateUrl: Global.clientPath + '/shared/not-authorized.html',
        data: { needAuthentication: false }
      });

    $urlRouterProvider.when('/app', Global.loginUrl);
    $urlRouterProvider.otherwise(Global.loginUrl);
  }
}());
