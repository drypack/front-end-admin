(function () {
  'use strict';

  angular
    .module('app')
    .config(routes);

  /**
   * Configure the route for dynamic query
   *
   * @param {any} $stateProvider
   * @param {any} Global
   */
  /** @ngInject */
  function routes($stateProvider, Global) {
    $stateProvider
      .state('app.dynamic-query', {
        url: '/dynamic-query',
        templateUrl: Global.clientPath + '/dynamic-query/dynamic-query.html',
        controller: 'DynamicQueryController as dynamicQueryCtrl',
        data: {
          needAuthentication: true,
          needPermission: {
            resource: 'dataInspection'
          }
        }
      });

  }
}());
