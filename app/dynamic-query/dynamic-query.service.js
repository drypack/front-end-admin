(function() {
  'use strict';

  angular
    .module('app')
    .factory('DynamicQueryService', DynamicQueryService);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function DynamicQueryService(serviceFactory) {
    return serviceFactory('dynamic-query', {
      /**
       * Custom action to return a list of avaible models
       */
      actions: {
        getModels: {
          method: 'GET',
          url: 'models'
        }
      },
      instance: {
      }
    });
  }

}());
