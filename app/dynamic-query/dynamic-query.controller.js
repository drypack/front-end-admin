(function() {

  'use strict';

  angular
    .module('app')
    .controller('DynamicQueryController', DynamicQueryController);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function DynamicQueryController($controller, DynamicQueryService, lodash, C2Toast, // NOSONAR
    $translate) {

    var vm = this;

    //actions
    vm.onActivate = onActivate;
    vm.applyFilters = applyFilters;
    vm.loadProps = loadProps;
    vm.loadOperators = loadOperators;
    vm.addFilter = addFilter;
    vm.afterSearch = afterSearch;
    vm.runFilter = runFilter;
    vm.editFilter = editFilter;
    vm.loadModels = loadModels;
    vm.removeFilter = removeFilter;
    vm.clear = clear;
    vm.restart = restart;


    // Inherits the base CRUD behaviors
    $controller('CRUDController', { vm: vm, modelService: DynamicQueryService, options: {
      searchOnInit: false
    } });

    function onActivate() {
      vm.restart();
    }

    /**
     * Prepares and applies the filters that will be included in the request
     *
     * @param {any} defaultQueryFilters
     * @returns
     */
    function applyFilters(defaultQueryFilters) {
      var where = {};

      /**
       * The service expects an object with:
       * - the model name
       * - a list of filters
       */
      if (vm.addedFilters.length > 0) {
        var addedFilters = angular.copy(vm.addedFilters);

        where.model = vm.addedFilters[0].model.name;

        for (var index = 0; index < addedFilters.length; index++) {
          var filter = addedFilters[index];

          //delete filter.model;
          filter.prop = filter.prop.name;
          filter.op = filter.op.value;
        }

        where.filters = angular.toJson(addedFilters);
      } else {
        where.model = vm.queryFilters.model.name;
      }

      return angular.extend(defaultQueryFilters, where);
    }

    /**
     * Load all the existing models in the back-end application
     */
    function loadModels() {
      // Retrive all the models from the server and mount a list for the combo box
      DynamicQueryService.getModels().then(function(data) {
        vm.models = data;
        vm.queryFilters.model = vm.models[0];
        vm.loadProps();
      });
    }

    /**
     * Load the properties of the chosen model
     */
    function loadProps() {
      vm.props = vm.queryFilters.model.props;
      vm.queryFilters.prop = vm.props[0];

      vm.loadOperators();
    }

    /**
     * Load the operators according the property data type
     */
    function loadOperators() {
      var operators = [
        { value: '=', label: $translate.instant('views.fields.queryDynamic.operators.equals') },
        { value: '<>', label: $translate.instant('views.fields.queryDynamic.operators.different') }
      ]

      if (vm.queryFilters.prop.type.indexOf('varying') !== -1 || vm.queryFilters.prop.type === 'string') {
        operators.push({ value: 'like',
          label: $translate.instant('views.fields.queryDynamic.operators.contains') });
        operators.push({ value: 'startswith',
          label: $translate.instant('views.fields.queryDynamic.operators.startswith') });
        operators.push({ value: 'endswith',
          label: $translate.instant('views.fields.queryDynamic.operators.endswith') });
      } else {
        operators.push({ value: '>',
          label: $translate.instant('views.fields.queryDynamic.operators.biggerThan') });
        operators.push({ value: '>=',
          label: $translate.instant('views.fields.queryDynamic.operators.equalsOrBiggerThan') });
        operators.push({ value: '<',
          label: $translate.instant('views.fields.queryDynamic.operators.lessThan') });
        operators.push({ value: '<=',
          label: $translate.instant('views.fields.queryDynamic.operators.equalsOrLessThan') });
      }

      vm.operators = operators;
      vm.queryFilters.op = vm.operators[0];
    }

    /**
     * Add/edits a filter
     *
     * @param {any} form form html element for validations
     */
    function addFilter(form) {
      if (angular.isUndefined(vm.queryFilters.value) || vm.queryFilters.value === '') {
        C2Toast.error($translate.instant('messages.validate.fieldRequired', { field: 'valor' }));
        return;
      } else {
        if (vm.index < 0) {
          vm.addedFilters.push(angular.copy(vm.queryFilters));
        } else {
          vm.addedFilters[vm.index] = angular.copy(vm.queryFilters);
          vm.index = -1;
        }

        // reset the form and the existing validations
        // vm.queryFilters = {};
        form.$setPristine();
        form.$setUntouched();
      }
    }

    /**
     * Realize a search having all the filters as parameters
     *
     */
    function runFilter() {
      vm.search(vm.paginator.currentPage);
    }

    /**
     * Trigger fired after the request that identifies the properties
     *
     * @param {any} data data result from the request
     */
    function afterSearch(data) {

      var keys = (data.items.length > 0) ? Object.keys(data.items[0]) : [];

      // Remove all properties that starts with $.
      // These properties are added by ther service and shall not appear in the list
      vm.keys = lodash.filter(keys, function(key) {
        return !lodash.startsWith(key, '$');
      })
    }

    /**
     * Put the chosen filter to edit in the form for editing
     *
     * @param {any} $index array index of the chosen filter
     */
    function editFilter($index) {
      vm.index = $index;
      vm.queryFilters = vm.addedFilters[$index];
    }

    /**
     * Remove a chosen filter
     *
     * @param {any} $index array index of the chosen filter
     */
    function removeFilter($index) {
      vm.addedFilters.splice($index);
    }

    /**
     * Clear the form
     */
    function clear() {
      // Keep the index of the register that is being edited
      vm.index = -1;
      // Link the form fields
      vm.queryFilters = {};
      if (vm.models) vm.queryFilters.model = vm.models[0];
    }

    /**
     * Reset the query filters
     *
     */
    function restart() {
      // Keep the properties of the current search
      vm.keys = [];

      // Keep the added filters
      vm.addedFilters = [];
      vm.clear();
      vm.loadModels();
    }
  }

})();
