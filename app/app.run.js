(function() {
  'use strict';

  angular
    .module('app')
    .run(run);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function run($rootScope, $state, $stateParams, Auth, Global) { // NOSONAR
    // The $state, $stateParams, Auth and Global are defined in the $rootScope
    // so they are accessible in the views without the controller prefix
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.auth = Auth;
    $rootScope.global = Global;

    // in the very beginning load the user from the localStorage (cover the case when the user reopens the browser/tab)
    Auth.retrieveUserFromLocalStorage();
  }
}());
