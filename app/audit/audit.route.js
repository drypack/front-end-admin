(function () {
  'use strict';

  angular
    .module('app')
    .config(routes);

  /**
   * Configure the route for laravel-audit
   *
   * @param {any} $stateProvider
   * @param {any} Global
   */
  /** @ngInject */
  function routes($stateProvider, Global) {
    $stateProvider
      .state('app.audit', {
        url: '/audit',
        templateUrl: Global.clientPath + '/audit/audit.html',
        controller: 'AuditController as auditCtrl',
        data: {
          needAuthentication: true,
          needPermission: {
            resource: 'audit'
          }
        }
      });

  }
}());
