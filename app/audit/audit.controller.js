(function () {

  'use strict';

  angular
    .module('app')
    .controller('AuditController', AuditController);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function AuditController($controller, AuditService, C2Dialog, Global, $translate) { // NOSONAR
    var vm = this;

    vm.onActivate = onActivate;
    vm.applyFilters = applyFilters;
    vm.viewDetail = viewDetail;

    $controller('CRUDController', {
      vm: vm,
      modelService: AuditService,
      options: {}
    });

    function onActivate() {
      vm.models = [];
      vm.queryFilters = {};

      // Retrive all the model's name and put them into a list
      AuditService.getAuditedModels().then(function (data) {
        var models = [{
          id: '',
          label: $translate.instant('global.all')
        }];

        data.models.sort();

        for (var index = 0; index < data.models.length; index++) {
          var model = data.models[index];

          models.push({
            id: model,
            label: $translate.instant('models.' + model.toLowerCase())
          });
        }

        vm.models = models;
        vm.queryFilters.model = vm.models[0].id;
      });

      vm.events = AuditService.listEvents();
      vm.queryFilters.event = vm.events[0].id;
    }

    /**
     * Apply filters
     *
     * @param {any} defaultQueryFilters
     * @returns
     */
    function applyFilters(defaultQueryFilters) {
      return angular.extend(defaultQueryFilters, vm.queryFilters);
    }

    /**
     * Show audit entry detail
     * @param {} auditDetail
     */
    function viewDetail(auditDetail) {
      var config = {
        locals: {
          auditDetail: auditDetail
        },
        /** @ngInject */
        controller: function (auditDetail, C2Dialog) {
          var vm = this;

          vm.close = close;

          activate();

          function activate() {
            if (angular.isArray(auditDetail.old_values) && auditDetail.old_values.length === 0) auditDetail.old_values = null;
            if (angular.isArray(auditDetail.new_values) && auditDetail.new_values.length === 0) auditDetail.new_values = null;

            vm.auditDetail = auditDetail;
          }

          function close() {
            C2Dialog.close();
          }

        },
        controllerAs: 'auditDetailCtrl',
        templateUrl: Global.clientPath + '/audit/audit-detail.html',
        hasBackdrop: true
      };

      C2Dialog.custom(config);
    }

  }

})();
