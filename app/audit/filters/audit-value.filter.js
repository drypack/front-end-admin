(function () {

  'use strict';

  angular
    .module('app')
    .filter('auditValue', auditValue);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function auditValue($filter, lodash, LocaleService) {
    return function (value, key) {
      if (angular.isDate(value) || lodash.endsWith(key, '_at') || lodash.endsWith(key, '_to')) {
        return $filter('c2Datetime')(value, LocaleService.getCurrentDateFormat());
      }

      if (typeof value === 'boolean') {
        return $filter('translate')((value) ? 'global.yes' : 'global.no');
      }

      // check is float
      if (Number(value) === value && value % 1 !== 0) {
        return $filter('currency')(value);
      }

      return value;
    }
  }

})();
