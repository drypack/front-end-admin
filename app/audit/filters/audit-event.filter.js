(function () {

  'use strict';

  angular
    .module('app')
    .filter('auditEvent', auditEvent);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function auditEvent(lodash, AuditService) {
    return function (eventId) {
      var event = lodash.find(AuditService.listEvents(), {
        id: eventId
      });

      return (event) ? event.label : event;
    }
  }
})();
