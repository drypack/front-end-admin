/*eslint angular/file-name: 0, no-undef: 0*/
(function () {
  'use strict';

  /**
   * Transforms the external libs in injectable angular services
   */
  angular
    .module('app')
    .constant('lodash', _)
    .constant('moment', moment);
}());
