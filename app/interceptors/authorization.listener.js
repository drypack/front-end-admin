(function () {
  'use strict';

  angular
    .module('app')
    .run(authorizationListener);

  /** @ngInject */
  function authorizationListener($rootScope, $state, Global, Auth) {
    /**
     * Check the permission in each state change
     */
    $rootScope.$on('$stateChangeStart', function (event, toState) {
      if (toState.data && toState.data.needAuthentication &&
        toState.data.needProfile && Auth.authenticated() &&
        !Auth.currentUser.hasPermission(toState.data.needPermission.resource, toState.data.needPermission.action)) {

        $state.go(Global.notAuthorizedState);
        event.preventDefault();
      }

    });
  }
}());
