(function () {
  'use strict';

  angular
    .module('app')
    .run(authenticationListener);

  /**
   * Listen all state (page) changes. Every time a state change we need to verify if the user is authenticated or not to
   * redirect to the correct page. When a user closes the browser without logout and reopens it this event
   * re-authenticate the user with the persistent token of the local storage.
   *
   * We don't check if the token is expired or not in the page change, because is generate an unecessary overhead.
   * If the token is expired when the user try to call the first api to get data, he/she will be logged off and redirected
   * to the login page.
   *
   * @param $rootScope
   * @param $state
   * @param $stateParams
   * @param Auth
   */
  /** @ngInject */
  // eslint-disable-next-line max-params
  function authenticationListener($rootScope, $state, Global, Auth, C2Toast, // NOSONAR
    $translate) {

    // only when application start it checks if the existing token is still valid
    Auth.remoteValidateToken().then(function () {
      // if the token is valid we check if the user exists, because the browser could have been closed
      // and the user data would not be in memory any more
      if (Auth.currentUser === null) {
        Auth.updateCurrentUser(angular.fromJson(localStorage.getItem('user')));
      }
    });

    // Check if the token still valid.
    $rootScope.$on('$stateChangeStart', function (event, toState) {
      if (toState.data.needAuthentication || toState.data.needProfile) {
        // dont trait the success block because already did by token interceptor
        Auth.remoteValidateToken().catch(function () {
          C2Toast.warn($translate.instant('messages.login.logoutInactive'));

          if (toState.name !== Global.loginState) {
            $state.go(Global.loginState);
          }

          event.preventDefault();
        });
      } else {
        // if the user is authenticated and need to enter in login page
        // he/she will be redirected to home page
        if (toState.name === Global.loginState && Auth.authenticated()) {
          $state.go(Global.homeState);
          event.preventDefault();
        }
      }
    });
  }
}());
