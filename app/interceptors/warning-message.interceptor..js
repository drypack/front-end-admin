(function () {
  'use strict';

  angular
    .module('app')
    .config(warningMessageInterceptor);

  function warningMessageInterceptor($httpProvider, $provide) {
    /**
     * This interceptor is responsible for showing the
     * http warning header or add it as pending to be show later
     *
     * @param {any} $q
     * @param {any} $injector
     * @returns
     */
    function showHeaderMessage($q, $injector) {
      return {
        response: function (response) {
          var C2Toast = $injector.get('C2Toast');
          var $translate = $injector.get('$translate');
          var Global = $injector.get('Global');

          var warningKey = response.headers('Warning');

          if (angular.isDefined(warningKey) && warningKey !== null) {
            var msgWarningKey = 'messages.' + warningKey;

            if (response.status === 200) {
              Global.pendingWarningKey = msgWarningKey;
            } else {
              C2Toast.warn($translate.instant(msgWarningKey));
            }
          }
          return $q.resolve(response);
        }
      };
    }

    // Define uma factory para o $httpInterceptor
    $provide.factory('showHeaderMessage', showHeaderMessage);

    // Push the new factory onto the $http interceptor array
    $httpProvider.interceptors.push('showHeaderMessage');
  }
}());
