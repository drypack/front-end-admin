(function () {

  'use strict';

  angular
    .module('app')
    .controller('AppController', AppController);

  /** @ngInject */
  /**
   * Controller responsible for functionalities that can be ran in any context
   *
   */
  function AppController($state, Auth, Global, LocaleService, C2Toast, $translate, $window, UsersService, $q) {
    var vm = this;

    // the current year to be shown in the admin footer
    vm.currentYear = null;

    vm.logout = logout;
    vm.getProfileImage = getProfileImage;
    vm.changeLocale = changeLocale;
    vm.isMultiLocale = isMultiLocale;

    activate();

    function activate() {
      var date = new Date();

      vm.currentYear = date.getFullYear();
      vm.locale = LocaleService.getCurrentLocale();
      vm.homeUrl = $window.location.origin + $window.location.pathname;
    }

    /**
     * Logout current user and redirect to the login state
     *
     * @return void
     */
    function logout() {
      Auth.logout().then(function () {
        $state.go(Global.loginState);
      });
    }

    /**
     * Change the locale based in the vm.locale model value
     * redefining all the translations based in web app translations
     * and remote translations that are retrieved and merged with web app translations
     *
     * @return void
     */
    function changeLocale() {
      var promises = [];

      // add the set locale promise
      promises.push(LocaleService.setLocale(vm.locale));

      // if there is a user authenticated,
      // we also update the back-end user language
      if (Auth.authenticated() === true) {

        // set the chosen locale as the user's locale
        Auth.currentUser.locale = vm.locale;
        // add the second promise, which will update the user locale in the back-end
        promises.push(UsersService.updateProfile(Auth.currentUser));
      }

      // when all promises are done, update locally the current user (with the new locale)
      // reload the state (so the new language is applied) and show a toast saying that the
      // language was switched
      $q.all(promises).then(function () {
        Auth.updateCurrentUser(Auth.currentUser);
        $state.reload();
        C2Toast.info($translate.instant('messages.localeSwitchedTo', {
          locale: vm.locale.toUpperCase()
        }));
      });
    }

    /**
     * Determine if the web app is multi language
     *
     * @returns
     */
    function isMultiLocale() {
      return LocaleService.isMultiLocale();
    }

    /**
     * Change the locale
     *
     * @return void
     */
    function getProfileImage() {
      return (Auth.currentUser && Auth.currentUser.image) ?
        Auth.currentUser.image :
        Global.imagePath + '/no_avatar.png';
    }

  }

})();
