/*eslint-env es6*/

(function() {

  'use strict';

  angular
    .module('app')
    .controller('MenuController', MenuController);

    /** @ngInject */
    // eslint-disable-next-line max-params
  function MenuController($mdSidenav, $state, $mdColors, Auth, MenuStates) {
    var vm = this;

    // Public functions
    vm.open = open;
    vm.openMenuOrRedirectToState = openMenuOrRedirectToState;
    vm.isOpen = isOpen;
    vm.close = close;
    vm.hasPermission = hasPermission;

    activate();

    function activate() {

      // Array with the menu items that are declared in the MenuStates constant
      vm.itemsMenu = MenuStates;

      /**
       * Object that fills the ng-style color and style attributes
       */
      vm.sidenavStyle = {
        top: {
          'border-bottom': '1px solid ' + getColor('primary'),
          'background-image': '-webkit-linear-gradient(top, ' + getColor('primary-500') + ', ' + getColor('primary-800') + ')'
        },
        content: {
          'background-color': getColor('primary-800')
        },
        textColor: {
          color: '#FFF'
        },
        lineBottom: {
          'border-bottom': '1px solid ' + getColor('primary-400')
        }
      }
    }

    function open() {
      $mdSidenav('left').toggle();
    }


    function isOpen() {
      var sidenav = $mdSidenav('left');

      if (angular.isDefined(sidenav)) {
        return sidenav.isLockedOpen() || sidenav.isOpen();
      }
      return false;
    }

    /**
     * Show the sub menu items or redirect to the item state if it has no subitems
     */
    function openMenuOrRedirectToState($mdMenu, ev, item) {
      if (angular.isDefined(item.subItems) && item.subItems.length > 0) {
        $mdMenu.open(ev);
      } else {
        $mdSidenav('left').close();
        $state.go(item.state);
      }
    }

    function getColor(colorPalettes) {
      return $mdColors.getThemeColor(colorPalettes);
    }

    /**
     * Verifies if the current user has permission to see a menu item
     * @param item menu item object containing the property needPermission and/or profiles
     */
    function hasPermission(item) {
      if (angular.isUndefined(item.needPermission)) {
        return true;
      } else if (angular.isUndefined(Auth.currentUser)) {
        return false;
      } else {
        return !Auth.currentUser.hasRole(item.roles, true) && Auth.currentUser.hasPermission(item.needPermission.resource, item.needPermission.action);
      }
    }
  }
})();
