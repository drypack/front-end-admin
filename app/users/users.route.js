(function () {
  'use strict';

  angular
    .module('app')
    .config(routes);

  /**
   * Configure the route for user resource
   *
   * @param {any} $stateProvider
   * @param {any} Global
   */
  /** @ngInject */
  function routes($stateProvider, Global) {
    $stateProvider
      .state('app.user', {
        url: '/users',
        templateUrl: Global.clientPath + '/users/users.html',
        controller: 'UsersController as usersCtrl',
        data: {
          needAuthentication: true,
          needPermission: {
            resource: 'users'
          }
        }
      })
      .state('app.user-profile', {
        url: '/user/profile',
        templateUrl: Global.clientPath + '/users/profile.html',
        controller: 'ProfileController as profileCtrl',
        data: {
          needAuthentication: true
        }
      });

  }
}());
