(function() {
  'use strict';

  angular
    .module('app')
    .factory('UsersService', UsersService);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function UsersService(lodash, Global, serviceFactory) {
    return serviceFactory('users', {
      // when an user is instanciated without parameter it is gonna have the default values
      defaults: {
        roles: []
      },

      actions: {
        /**
         * Update the user profile in the back-end
         *
         * @param {object} attributes
         * @returns {promise}
         */
        updateProfile: {
          method: 'PUT',
          url: Global.apiPath + '/profile',
          override: true,
          wrap: false
        }
      },

      instance: {
        /**
         * Verifies if the user has the given profile(s) 
         *
         * @param {any} roles profile(s) to be verified
         * @param {boolean} all flag if must have all or just one of the given profiles
         * @returns {boolean}
         */
        hasRole: function(roles, all) {
          roles = angular.isArray(roles) ? roles : [roles];

          var userRoles = lodash.map(this.roles, 'slug');
          
          if (all) {
            return lodash.intersection(userRoles, roles).length === roles.length;
          } else { //return the length because 0 is false in js
            return lodash.intersection(userRoles, roles).length;
          }
        },

        /**
         * Verifies if the user has a given action permission in a given resource
         * If no action is passed, it will look for the 'index' action
         *
         * @param {any} resource to be verified
         * @param {any} action action to be verified
         * @returns {boolean}
         */
        hasPermission: function(resource, action) {
          // If no action is passed, it will look for the 'index' action permission (listing permission)
          action = action? action : 'index';

          var allowed = lodash.find(this.allowed_actions, function(a) {
            return a.action_type_slug === action && a.resource_slug === resource;
          })

          return allowed? true : false;
        },

        /**
         * Verifies if the user has the admin profile
         *
         * @returns {boolean}
         */
        isAdmin: function() {
          return this.hasRole('admin');
        }
      }
    });
  }

}());
