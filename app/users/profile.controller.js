(function() {

  'use strict';

  angular
    .module('app')
    .controller('ProfileController', ProfileController);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function ProfileController(UsersService, Auth, C2Toast, $translate) {
    var vm = this;

    vm.update = update;

    activate();

    function activate() {
      vm.user = angular.copy(Auth.currentUser);
    }

    function update() {
      UsersService.updateProfile(vm.user).then(function (response) {
        // updates the current user with fresh data
        Auth.updateCurrentUser(response);
        C2Toast.success($translate.instant('messages.saveSuccess'));
      });
    }
  }

})();
