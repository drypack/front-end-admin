(function () {

  'use strict';

  angular
    .module('app')
    .controller('PasswordController', PasswordController);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function PasswordController(Global, $stateParams, $http, $timeout, $state, // NOSONAR
    C2Toast, C2Dialog, Auth, $translate, LocaleService) {

    var vm = this;

    vm.sendReset = sendReset;
    vm.closeDialog = closeDialog;
    vm.cleanForm = cleanForm;
    vm.sendEmailResetPassword = sendEmailResetPassword;
    vm.changeLocale = changeLocale;
    vm.isMultiLocale = isMultiLocale

    activate();

    function activate() {
      vm.reset = { email: '', token: $stateParams.token };
      vm.locale = LocaleService.getCurrentLocale();
    }

    /**
     * Send the new password defined by the user to the back-end and redirect the user to home state
     */
    function sendReset() {
      $http.post(Global.apiPath + '/password/reset', vm.reset)
        .then(function () {
          C2Toast.success($translate.instant('messages.operationSuccess'));
          $timeout(function () {
            $state.go(Global.loginState);
          }, 1500);
        }, function (error) {
          if (error.status !== 400 && error.status !== 500) {
            var msg = '';

            for (var i = 0; i < error.data.password.length; i++) {
              msg += error.data.password[i] + '<br>';
            }
            C2Toast.error(msg.toUpperCase());
          }
        });
    }

    /**
     * Send an e-mail with the password reset link
     */
    function sendEmailResetPassword() {

      if (vm.reset.email === '') {
        C2Toast.error($translate.instant('messages.validate.fieldRequired', { field: 'email' }));
        return;
      }

      Auth.sendEmailResetPassword(vm.reset).then(function (data) {
        C2Toast.success(data.message);

        vm.cleanForm();
        vm.closeDialog();
      }, function (error) {
        if (error.data.email && error.data.email.length > 0) {
          var msg = '';

          for (var i = 0; i < error.data.email.length; i++) {
            msg += error.data.email[i] + '<br>';
          }

          C2Toast.error(msg);
        }
      });
    }

    function closeDialog() {
      C2Dialog.close();
    }

    function cleanForm() {
      vm.reset.email = '';
    }

    /**
     * Change the locale based in the vm.locale model value
     * redefining all the translations based in web app translations
     * and remote translations that are retrived and merged with web app translations
     *
     * @return void
     */
    function changeLocale() {
      LocaleService.setLocale(vm.locale).then(function() {
        $state.reload();
      });
    }

     /**
     * Determine if the web app is multi language
     *
     * @returns
     */
    function isMultiLocale() {
      return LocaleService.isMultiLocale();
    }

  }

})();
