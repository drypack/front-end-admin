(function() {

  'use strict';

  angular
    .module('app')
    .controller('LoginController', LoginController);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function LoginController($state, Auth, Global, C2Dialog, LocaleService) {
    var vm = this;

    vm.login = login;
    vm.openDialogResetPass = openDialogResetPass;
    vm.changeLocale = changeLocale;
    vm.isMultiLocale = isMultiLocale

    activate();


    function activate() {
      vm.credentials = {};
      vm.locale = LocaleService.getCurrentLocale();
    }
    /**
     * Login the user in the back-end and goto to home state
     */
    function login() {
      var credentials = {
        email: vm.credentials.email,
        password: vm.credentials.password
      };

      Auth.login(credentials).then(function() {
        $state.go(Global.homeState);
      });
    }

    /**
     * Show a recovery password dialog
     */
    function openDialogResetPass() {
      var config = {
        templateUrl: Global.clientPath + '/auth/send-reset-dialog.html',
        controller: 'PasswordController as passCtrl',
        hasBackdrop: true
      }

      C2Dialog.custom(config);
    }

    /**
     * Change the locale based in the vm.locale model value
     * redefining all the translations based in web app translations
     * and remote translations that are retrived and merged with web app translations
     *
     * @return void
     */
    function changeLocale() {
      LocaleService.setLocale(vm.locale).then(function() {
        $state.reload();
      });
    }

     /**
     * Determine if the web app is multi language
     *
     * @returns
     */
    function isMultiLocale() {
      return LocaleService.isMultiLocale();
    }

  }

})();
