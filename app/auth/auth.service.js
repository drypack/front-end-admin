(function() {
  'use strict';

  angular
    .module('app')
    .factory('Auth', Auth);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function Auth($http, $q, Global, UsersService) { // NOSONAR
    var auth = {
      login: login,
      logout: logout,
      updateCurrentUser: updateCurrentUser,
      retrieveUserFromLocalStorage: retrieveUserFromLocalStorage,
      authenticated: authenticated,
      sendEmailResetPassword: sendEmailResetPassword,
      remoteValidateToken: remoteValidateToken,
      getToken: getToken,
      setToken: setToken,
      clearToken: clearToken,
      getAuthenticatedUser: getAuthenticatedUser,
      currentUser: null
    };

    function clearToken() {
      localStorage.removeItem(Global.tokenKey);
    }

    function setToken(token) {
      if (angular.isDefined(token) && token !== null) {
        localStorage.setItem(Global.tokenKey, token);
      }
    }

    function getToken() {
      return localStorage.getItem(Global.tokenKey);
    }


    function remoteValidateToken() {
      var deferred = $q.defer();

      if (auth.authenticated()) {
        $http.get(Global.apiPath + '/authenticate/check')
          .then(function() {
            deferred.resolve(true);
          }, function() {
            auth.logout();

            deferred.reject(false);
          });
      } else {
        auth.logout();

        deferred.reject(false);
      }

      return deferred.promise;
    }

    /**
     * Verifies if a user is authenticated
     *
     * @return {boolean}
     */
    function authenticated() {
      return auth.getToken() !== null
    }

    /**
     * Retrive the authenticated user from localStorage
     */
    function retrieveUserFromLocalStorage() {
      var user = localStorage.getItem('user');

      if (user) {
        auth.currentUser = angular.merge(new UsersService(), angular.fromJson(user));
      }
    }

    /**
     * Keep the user in the localStorage, in the case when the browser is closed and reopened
     * during the period of validation of the token
     *
     * Keep the user in the attribute auth.currentUser to easy the access to the current logged user
     *
     *
     * @param {any} user User to be updated
     */
    function updateCurrentUser(user) {
      var deferred = $q.defer();

      if (user) {
        user = angular.merge(new UsersService(), user);

        var jsonUser = angular.toJson(user);

        localStorage.setItem('user', jsonUser);
        auth.currentUser = user;

        deferred.resolve(user);
      } else {
        localStorage.removeItem('user');
        auth.currentUser = null;
        auth.clearToken();

        deferred.reject();
      }

      return deferred.promise;
    }

    /**
     * Login a user
     *
     * @param {any} credentials containing the properties email and password
     * @returns {promise} a response from the request to the back-end
     */
    function login(credentials) {
      var deferred = $q.defer();

      $http.post(Global.apiPath + '/authenticate', credentials)
        .then(function(response) {
          auth.setToken(response.data.token);

          return getAuthenticatedUser();
        })
        .then(function(response) {
          auth.updateCurrentUser(response.data.user);

          deferred.resolve();
        }, function(error) {
          auth.logout();

          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getAuthenticatedUser() {
      return $http.get(Global.apiPath + '/authenticate/user');
    }

    /**
     * Logout the user, removing the user from the browser local storage and redirect to the login page
     *
     * @returns {promise} a promise with the result
     */
    function logout() {
      var deferred = $q.defer();

      auth.updateCurrentUser(null);
      deferred.resolve();

      return deferred.promise;
    }

    /**
     * Make a request to the back-end to send a reset password link to the registered email
     *
     * @param {Object} resetData - object containing the email property
     * @return {Promise} - a request promise
     */
    function sendEmailResetPassword(resetData) {
      var deferred = $q.defer();

      $http.post(Global.apiPath + '/password/email', resetData)
        .then(function(response) {
          deferred.resolve(response.data);
        }, function(error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    return auth;
  }

}());
