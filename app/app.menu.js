/*eslint angular/file-name: 0, no-undef: 0*/
(function () {
  'use strict';

  // prefix used to all menu items
  // dont remove this unless you know waht you are doing!
  var menuPrefix = 'views.layout.menu.';

  angular
    .module('app')
    .constant('MenuStates', [

      /*
      |--------------------------------------------------------------------------
      | Front-end application menu definition
      |--------------------------------------------------------------------------
      |
      | Put here the menu items and subitems of the application using the examples
      | You can add/change all the native menu items, but then the corresponding functionalities
      | will not be accessible in the front-end
      */

      // Dashboard, the default state after loging in
      {
        state: 'app.dashboard',
        title: menuPrefix + 'dashboard',
        icon: 'dashboard'
      },

      // DryPack sample functionality
      {
        state: '#',
        title: menuPrefix + 'examples',
        icon: 'view_carousel',
        subItems: [{
          state: 'app.project',
          title: menuPrefix + 'project',
          icon: 'donut_large',
          needPermission: {
            resource: 'project'
          }
        }]
      },
      // Put here your custom menu items
      {
        state: '#',
        title: menuPrefix + 'admin',
        icon: 'settings_applications',
        roles: ['admin'],
        subItems: [{
            state: 'app.user',
            title: menuPrefix + 'user',
            icon: 'people',
            needPermission: {
              resource: 'users'
            }
          },
          {
            state: 'app.mail',
            title: menuPrefix + 'mail',
            icon: 'mail',
            needPermission: {
              resource: 'emails',
              action: 'store'
            }
          },
          {
            state: 'app.roles',
            title: menuPrefix + 'roles',
            icon: 'perm_contact_calendar',
            needPermission: {
              resource: 'role'
            }
          },
          {
            state: 'app.audit',
            title: menuPrefix + 'audit',
            icon: 'storage',
            needPermission: {
              resource: 'audit'
            }
          },
          {
            state: 'app.dynamic-query',
            title: menuPrefix + 'dynamicQuery',
            icon: 'location_searching',
            needPermission: {
              resource: 'dataInspection'
            }
          }
        ]
      }
    ]);
}());
