(function () {
  'use strict';

  angular
    .module('app')
    .constant('Global', {
      appName: 'DryPack',
      homeState: 'app.dashboard',
      loginUrl: 'app/login',
      loginState: 'app.login',
      resetPasswordState: 'app.password-reset',
      notAuthorizedState: 'app.not-authorized',
      tokenKey: 'server_token',
      srcBasePath: '/admin',
      clientPath: '/admin/app',
      apiPath: '/api/v1',
      imagePath: '/admin/images',
      locales: [
        { id: 'pt-BR', dtFormat: 'DD/MM/YYYY', tmFormat: 'HH:mm' },
        { id: 'en-US', dtFormat: 'YYYY/MM/DD', tmFormat: 'HH:mm' }
      ],
      defaultLocale: 'en-US'
    });
}());

