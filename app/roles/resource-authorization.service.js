(function() {
  'use strict';

  angular
    .module('app')
    .factory('ResourceAuthorizationService', ResourceAuthorizationService);

  /** @ngInject */
  function ResourceAuthorizationService(serviceFactory) {
    var model = serviceFactory('authorization/resources', {
      actions: { },
      instance: { }
    });

    return model;
  }

}());
