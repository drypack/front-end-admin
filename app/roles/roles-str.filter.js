(function() {

  'use strict';

  angular
    .module('app')
    .filter('rolesStr', rolesStr);

  /** @ngInject */
  function rolesStr(lodash) {
    /**
     * @param {array} roles roles list
     * @return {string} roles separed by ', '
     */
    return function(roles) {
      return lodash.map(roles, 'slug').join(', ');
    };
  }

})();
