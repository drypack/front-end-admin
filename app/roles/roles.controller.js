/*
 * This file is part of the DryPack Dynamic Authorization
 *
 * @author Amon Santana <amoncaldas@gmail.com>
 */

(function () {
  'use strict'

  angular
    .module('app')
    .controller('RolesController', RolesController)

  /** @ngInject */
  // eslint-disable-next-line max-params
  function RolesController ($controller, RolesService, ResourceAuthorizationService, lodash, C2Toast, $translate, Auth) {
    var vm = this;

    // public functions
    vm.applyFilters = applyFilters;
    vm.afterEdit = afterEdit;
    vm.afterClean = afterClean;
    vm.afterSave = afterSave;
    vm.actionsSelectionChanged = actionsSelectionChanged;

    // Functions Block

    // instantiate base controller
    $controller('CRUDController', { vm: vm, modelService: RolesService, options: { } })

    vm.onActivate = function () {
      vm.actionsBeforeChange = [];
    }

    /**
     * Apply the filter to get all the roles
     *
     * @param {defaultQueryFilters}
     */
    function applyFilters(defaultQueryFilters) {
      return angular.extend(defaultQueryFilters, { includeAnonymous: true });
    }

    /**
     * Handle the event of action selection change in a single resource
     *
     * Case 1: when the resource is a wildcard (all) and the action is also a wildcard (all)
     *  change the selection of all the actions in all the resources
     * Case 2: when only the resource is a wildcard, but the action ins't a wildcard
     *  change the selection of all actions in all resources
     * Case 3: when the wildcard action of a concrete resource is changed,
     *  change all the actions of the resource
     * Case 4: when a concrete action of a concrete resource is changed
     *  change only the action changed
     *
     * @param {currentResource} currentResource resource in which the action has changed
     */
    function actionsSelectionChanged(currentResource) {

      // Retrieve the action changed
      var actionChanged = getActionChanged();

      // Retrieve the resource wildcard from the possible list
      var wildcardResource = lodash.find(vm.availableResources, function (ar) {
        return ar.slug === 'all'
      });

      // If the action modified is from a global resource wildcard resource = all && action == all
      // if yes, the change is applied to all the actions of all the resources
      if (wildcardResource.slug === currentResource.slug) {
        if (actionChanged !== false) {
          triggerAllSelectionForSingleResource(currentResource, actionChanged);
        }
        triggerAllSelectionForResources(wildcardResource);

      } else if (actionChanged !== false) { // if an action was changed

        // If the action changed is a resource wildcard
        // all the action from the resource are changed
        triggerAllSelectionForSingleResource(currentResource, actionChanged);
      }

      // Apply change in the dependence actions of the changed action
      if (actionChanged !== false) {
        triggerActionDependenciesSelection(currentResource);
      }

      // Re-analyze if the wildcard 'all' must be kept selected
      // this is needed when when 'all' was selected and a single action is unselected
      // so it does not make sense to keep the 'all' selected
      if (actionChanged !== false) {
        adjustTheAllSelection(currentResource, wildcardResource, actionChanged);
      }

      // After all treatments, the current list of selected actions is cloned
      // to be used as last state in a new change cycle
      vm.actionsBeforeChange = angular.copy(vm.resource.actions);
    }

    /**
     * Verifies if the unselected action multi be really unselected
     * it is necessary when the wildcard 'all' were selected and one of the concrete actions is unselected
     * or when the only one missing action is selected (then the all must be auto selected
     * @param object wildcardResource abstract resource used to propagate action selection
     * @param object currentResource resource in which must be selected the abstract action 'all'
     * @param object actionChanged action that was modified (selected or unselected)
     */
    function adjustTheAllSelection(currentResource, wildcardResource, actionChanged) {
      // Case 1: if every action is selected, except 'all', so we can keep 'all' as unselected

      // Case 2: if there are items not selected besides the wildcard 'all'
      // then the 'all' must be kept unselected.

      // Case 3: if all the actions are selected and an item was modified
      // then an unselect action is occurring

      // Conclusion: when the wildcard 'all' is unselected, it can be kept unselected in all the cases
      // Only when an action is being marked as unselected we have to do a custom step

      if (actionChanged.action_type_slug !== 'all') {
        // in the first case the wildcard resource 'all' is being modified
        if (wildcardResource.slug === currentResource.slug) {
          // Tries to retrieve the wildcard action from the wildcard resource
          var actionAllInWildcardResource = lodash.find(vm.resource.actions, function (a) {
            return a.action_type_slug === 'all' && a.resource_slug === wildcardResource.slug
          })

          // If it is selected, unselect it (removing from the selected list)
          if (actionAllInWildcardResource) {
            lodash.remove(vm.resource.actions, function (a) {
              return a.id === actionAllInWildcardResource.id
            })
          }
        } else { // else, the wildcard action of a concrete resource is being handled
          // unselect this concrete resource wildcard
          lodash.remove(vm.resource.actions, function (a) {
            return a.action_type_slug === 'all' && a.resource_slug === currentResource.slug
          });
          // unselect the global abstract resource wildcard
          lodash.remove(vm.resource.actions, function (a) {
            return a.action_type_slug === 'all' && a.resource_slug === wildcardResource.slug
          });

          // if a concrete action is unselected, so we must unselect the global wildcard 'all'
          if (!actionChanged.wasAdded) {
            lodash.remove(vm.resource.actions, function (a) {
              return a.action_type_slug === actionChanged.action_type_slug && a.resource_slug === wildcardResource.slug
            })
          }
        }
      }
    }


    /**
     * Retrieve the changed action
     * @return object actionChanged
     */
    function getActionChanged() {
      // retrieve the difference between the last actions state and the current, using XOR
      var actionsDiff = lodash.xorBy(vm.resource.actions, vm.actionsBeforeChange, 'id')
      var actionDiff = null;

      // As the verification is done every time an action change, the difference must be, in principle, of 1
      // and the attribute 'wasAdded' is initialized as false
      if (actionsDiff.length === 1) {
        actionDiff = actionsDiff[0];
        actionDiff.wasAdded = false;
      }
      // if no difference was found, return false
      if (angular.isUndefined(actionDiff) || actionDiff === null) {
        return false;
      }

      // If the difference represents that and action has been added
      // (the diff action is in the current collection of actions)
      // wasAdded is defined as true;
      if (actionDiff) {
        var added = lodash.filter(vm.resource.actions, function (a) {
          return a.id === actionDiff.id
        });

        if (added && added.length > 0) {
          actionDiff.wasAdded = true;
        }
      }

      return actionDiff
    }

    /**
    * Propagate the wildcard selected action to the concrete resources
    *
    * @param object wildcardResource resource containing the actions that must be propagated
    */
    function triggerAllSelectionForResources (wildcardResource) {
      angular.forEach(vm.availableResources, function (availableResource, index) {
        // the actions are propagated only if the changed resource is the wildcard 'all'
        if (availableResource.slug !== wildcardResource.slug) {

          // retrieve the actions that must be propagated
          var propagableActions = lodash.filter(vm.resource.actions, function (a) {
            return a.resource_slug === wildcardResource.slug
          });

          // apply these actions state (select/unselected) to the concrete resources
          angular.forEach(propagableActions, function (action) {
            selectActions(vm.availableResources[index], action.action_type_slug)
          })
        }
      });
    }

    /**
    * Propagate the wildcard action 'all' change of a single resource to every action of the resource
    *
    * @param object currentResource in which the wildcard must propagated
    * @param object actionChanged
    */
    function triggerAllSelectionForSingleResource(currentResource, actionChanged) {
      if (actionChanged !== false && actionChanged.wasAdded === true) {
        // Here we check if the action that is being propagated belongs to the global wildcard resource
        // If yes, this means that all actions must be applied to current resource
        var actionAllToCurrentResource = lodash.find(vm.resource.actions, function (a) {
          return a.action_type_slug === 'all' && a.resource_slug === currentResource.slug
        })

        // apply the wildcard state to the resource actions
        if (actionAllToCurrentResource && actionChanged.wasAdded) {
          selectActions(currentResource, 'all')
        }
      }
    }

    /**
    * Run the dependence verification and select the dependent actions for a given action
    * When a dependent action is selected, show a toaster to notify the user about it
    * @param object currentResource
    */
    function triggerActionDependenciesSelection (currentResource) {
      // retrieve the actions of the resource that were selected
      var resourceActionsSelected = lodash.intersectionBy(currentResource.actions, vm.resource.actions, 'id')

      // get, among all changed actions, the actions that have dependencies
      var actionsWithDependencies = lodash.filter(resourceActionsSelected, function (a) {
        return a.dependencies.length > 0
      })

      // if there are actions with dependencies, we retrieve the target actions and select them
      if (actionsWithDependencies.length > 0) {
        angular.forEach(actionsWithDependencies, function (action) {
          angular.forEach(action.dependencies, function (actionD) {
            var resourceDestinationIndex = lodash.findIndex(vm.availableResources, function (r) {
              return r.slug === actionD.resource_slug
            })

            selectActions(vm.availableResources[resourceDestinationIndex], actionD.action_type_slug)
          })

          // show a notification about the auto selected actions
          notifyAutoDependenciesSelection(currentResource, action)
        })
      }
    }


    /**
    * Notifies the user, with a toaster about the dependence auto selection
    *
    * @param object resource in which the the dependencies were auto selected
    * @param object action which was auto selected
    */
    function notifyAutoDependenciesSelection (resource, action) {
      var params = { action: action.action_type_name + ' ' + resource.name };

      C2Toast.info($translate.instant('messages.actionDependenciesAutoSelected', params))
    }

    /**
    * Select an action in a given resource
    * If the given action is the wildcard 'all', select all the actions in the given resource
    *
    * @param object resource in which the actions must de selected
    * @param string actionSlug of the action that must be selected
    */
    function selectActions (resource, actionSlug) {
      // If the given action is the wildcard 'all', select all the actions in the given resource
      if (actionSlug === 'all') {
        angular.forEach(resource.actions, function (action, index) {
          var currentActionAlreadyAdded = lodash.find(vm.resource.actions, function (a) {
            a.id === action.id
          });

          if (!currentActionAlreadyAdded) {
            vm.resource.actions.push(resource.actions[index])
          }
        })
      } else { // if not, the specified action is applied
        var actionIndex = lodash.findIndex(resource.actions, ['action_type_slug', actionSlug])

        if (actionIndex > 0) {
          vm.resource.actions.push(resource.actions[actionIndex])
        }
      }
      // We make sure that there no duplicated action on the list
      vm.resource.actions = lodash.uniqBy(vm.resource.actions, 'id');
    }


    /**
     * Adjust rhe models and controller attributes after loading the edit state
     * @return void
     */
    function afterEdit() {
      vm.actionsBeforeChange = []
      if (vm.resource.actions) {
        vm.actionsBeforeChange = angular.copy(vm.resource.actions);
      }

      // get all available resources
      ResourceAuthorizationService.query({ slug: vm.resource.slug }).then(function (results) {
        vm.availableResources = results
      })
    }

    /**
     * Reset the resource and actions
     */
    function afterClean() {
      vm.resource.name = null
      vm.resource.actions = []
      vm.actionsBeforeChange = []
    }

    /**
     * After save, refresh the current user if the current user has the role updated
     */
    function afterSave() {
      if (Auth.currentUser.hasRole(vm.resource.slug)) {
        Auth.getAuthenticatedUser().then(function(response) {
          Auth.updateCurrentUser(response.data.user);
        })
      }
    }
  }
})()
