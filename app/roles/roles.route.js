/*
 * This file is part of the DryPack Dynamic Authorization
 *
 * @author Amon Santana <amoncaldas@gmail.com>
 */

(function () {
  'use strict';

  angular
    .module('app')
    .config(routes);

  /**
   * Configure the route for the mail sending
   *
   * @param {any} $stateProvider
   * @param {any} Global
   */
  /** @ngInject */
  function routes($stateProvider, Global) {
    $stateProvider
      .state('app.roles', {
        url: '/roles',
        templateUrl: Global.clientPath + '/roles/roles.html',
        controller: 'RolesController as rolesCtrl',
        data: {}
      });
  }
}());
