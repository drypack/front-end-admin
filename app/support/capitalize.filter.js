(function() {

  'use strict';

  angular
    .module('app')
    .filter('capitalize', capitalize);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function capitalize($filter) {
    /**
     * Filter for capitalizing
     *
     * @param string str string to be capitalized
     * @returns capitalized string
     */
    return function(str) {
      if (angular.isUndefined(str)) str = '';
      var capitalized = str.toUpperCase();
      return capitalized;
    }
  }
})();