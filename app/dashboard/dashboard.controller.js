(function() {

  'use strict';

  angular
    .module('app')
    .controller('DashboardController', DashboardController);

  /** @ngInject */
  /**
   * Dashboard Controller
   *
   * Default user page after login
   *
   */
  function DashboardController() {
    // Empty controller. It can be customized adding graphics and statistics, for example
  }

})();
